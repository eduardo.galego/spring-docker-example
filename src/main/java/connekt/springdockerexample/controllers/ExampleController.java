package connekt.springdockerexample.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ExampleController {

    @ResponseBody
    @RequestMapping("/echo")
    public String echo(@RequestParam(value="user", defaultValue="user") String user) {
        return "All working in dev environment, " + user;
    }

}
